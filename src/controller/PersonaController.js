import config from '../config.js';
import axios from 'axios';

function savePersona(persona, cb, err) {
	axios.post(config.api.persona,persona)
    .then(res=> {
        cb(res.data)
      })
      .catch(error=> {
        err(error)
      });
}

function getPersona(persona_id, cb, err) {
	axios.get(config.api.persona + persona_id)
    .then(res=> {
        cb(res.data)
      })
      .catch(error=> {
        err(error)
      });
}

function deletePersona(persona_id, cb, err) {
	axios.delete(config.api.persona + persona_id)
    .then(res=> {
        cb(res.data)
      })
      .catch(error=> {
        err(error)
      });
}

function findAllPersona(cb, err) {
	axios.get(config.api.persona)
    .then(res=> {
        cb(res.data)
      })
      .catch(error=> {
        err(error)
      });
}

export default {
    savePersona:savePersona,
    getPersona:getPersona,
    deletePersona:deletePersona,
    findAllPersona:findAllPersona
}