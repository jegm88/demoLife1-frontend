import Vue from 'vue'
import App from './App.vue'

//Imports boostrap
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'bootstrap/dist/css/bootstrap.css';

//Use boostrap
Vue.use(BootstrapVue);

new Vue({
  el: '#app',
  render: h => h(App)
})
